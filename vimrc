" The best VIM config

" essencials
filetype on                         " enables filetype detection
filetype plugin on                  " enables filetype specific plugins
set encoding=utf-8                  " use utf-8 encoding
let mapleader=","                   " map leader to ,
set clipboard=unnamed               " share clipboard
set backspace=indent,eol,start      " enables backspacing
inoremap jk <ESC>                   " remap the exit button ESC too far from home row


" Color configuration
syntax enable
"set background=dark
set background=light
" let g:solarized_termcolors = 256
colorscheme solarized
"call togglebg#map("<F12>")           " toggle background color change


" TAB configuration
set tabstop=4                       " number of visual spaces per TAB
set softtabstop=4                   " number of spaces in tab when editing
set expandtab                       " tabs are spaces
set shiftwidth=4                    " autoindent to 4 spaces


" Turn on invisibles
"set list listchars=tab:▷⋅,trail:⋅,nbsp:⋅


" UI Config
set number                          " show line number
set showcmd                         " show last command in botom bar
set cursorline                      " highlight the selected line
set cursorcolumn                    " highlight the selected column
filetype indent on                  " load filetype-specific indent files from ~/.vim/indent/*
set wildmenu                        " visual autocomplete for command menu
set lazyredraw                      " redraw only when necessary, optimization
set showmatch                       " highlight matching [{()}]
set splitbelow                      " open split below current window
set splitright                      " open vertical split at the right of current window
" Navigate through splits with one combo
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>
nnoremap <C-h> <C-w><C-h>

" POWERLINE configuration
set rtp+=/usr/local/lib/python3.7/site-packages/powerline/bindings/vim
set laststatus=2
set t_Co=256


" Searching
set incsearch                       " search as characters are entered
set hlsearch                        " highlight matches

" turn off search highlights
nnoremap <leader><space> :nohlsearch<CR>


" Folding
set foldenable                      " enable folding
set foldlevelstart=10               " open most folds by default
set foldnestmax=10                  " 10 nested fold max
" space open/close folds
"nnoremap <space> za
set foldmethod=indent
let g:SimpylFold_docstring_preview = 1   " Show docstring preview

" Movement Configuration
" move vertically by visual line
nnoremap j gj
nnoremap k gk


" NERDTree Configuration
" Start NERDTree automatically when no file specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Open NERDTree when the target is a directory
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
" Open NERDTree with CTRL-n
map <C-n> :NERDTreeToggle<CR>
" Close NERDTree when it is the last window left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"let NERDTreeQuitOnOpen = 1          " automatically close NERDTree when open file
let NERDTreeAutoDeleteBuffer = 1    " automatically delete buffer when file removed with NERDTree
let NERDTreeMinimalUI = 1           " disable help
let NERDTreeDirArrows = 1           " show directory arrows

" Tags
set tags=.tags
nnoremap <F8> :TagbarToggle<CR>


" ALE Configuration
nnoremap <leader><C-l> :ALEFix<CR>


" Ack configuration
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

nnoremap <C-S-f> :Ack<CR>

" Undo tree
nnoremap <F5> :UndotreeToggle<cr>


