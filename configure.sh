#!/bin/bash
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=$(realpath $0)
SCRIPTPATH="$(dirname "$SCRIPT")"


ln -sfn "${SCRIPTPATH}/" ~/.vim
ln -sfn "${SCRIPTPATH}/vimrc" ~/.vimrc
mkdir colors syntax
ln -sfn "${SCRIPTPATH}/pack/my_plugins/start/solarized/colors/solarized.vim" ~/.vim/colors/solarized.vim
ln -sfn "${SCRIPTPATH}/pack/my_plugins/start/vim-gradle/compiler/gradle.vim" ~/.vim/syntax/gradle.vim
